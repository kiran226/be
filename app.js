require('dotenv').config();

const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const cookieParser = require("cookie-parser");
const cors = require("cors");

const authRoutes = require("./routes/auth");

//DB Connection
mongoose.connect( process.env.DATABASE , 
{ 
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true
 }).then(()=>{
     console.log(`DB connected`)
 });

 //middlewares
 app.use(express.json());
 app.use(cookieParser());
 app.use(cors());

 //Routes
app.use("/api", authRoutes);

//PORT
const port=process.env.PORT|| 8000;

//starting a server
app.listen(port, ()=>{
    console.log(`App is running at ${port}`)
})
